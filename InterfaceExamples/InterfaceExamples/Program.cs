﻿///The using directive can be used in several places, but this is the most common
///It tells the CSharp build system that we want to use classes that are defined in
///another namespace. Other apps can reference our app by using the following using statement
///using InterfaceExamples;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

///This is a namespace definition and it allows other programs to use our program 
///without confusing the types in their program with types in our program
namespace InterfaceExamples
{
	/// <summary>
	/// The following line of code is a class definition
	/// Although this class has no explicit properties or methods aside from "Main",
	/// it is still considered a class definition.
	/// </summary>
	class Program
	{
		/// <summary>
		/// The Main method is a STATIC method that can be accessed globally by calling
		/// InterfaceExamples.Program.Main();
		/// In fact, this is how the C# system knows how to find the entry point for your application
		/// </summary>
		/// <param name="args">The Main method can either have a string-array or nothing as parameters</param>
		/// <returns>Any command-line program can return void or an int when it is done executing. The return code can signal failure if necessary</returns>
		static int Main(string[] args)
		{
			//Write 'Hello, World!' to the command-line and move the cursor to the next line
			Console.WriteLine("Hello, World!");

			//Wait for the user to hit a key to exit the program
			Console.ReadLine();

			//This version of Hello, World returns 0 to denote Success
			return 0;
		}
	}
}
